<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>TASK MANAGER</title>
</head>
<body>
<table width="100%" height="100%" border="1" style="border-collapse: collapse;">
    <tr>
        <td height="35" width="200" nowrap="nowrap" align="center">
            <a href="/"><b>TASK MANAGER</b></a>
        </td>

        <td width="100%" align="right">
            <a href="/projects">PROJECTS</a>

            &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/tasks">TASKS</a>
            &nbsp;&nbsp;

            <sec:authorize access="isAuthenticated()">
                USER: <sec:authentication property="name"/>
                &nbsp;&nbsp;
                <a href="/logout">LOGOUT</a>
                &nbsp;&nbsp;
            </sec:authorize>

            <sec:authorize access="!isAuthenticated()">
                <a href="/login">LOGIN</a>
                &nbsp;&nbsp;
            </sec:authorize>

        </td>
    </tr>
    <tr>
        <td colspan="2" heigh="100%" valign="top" style="padding: 10px">

package com.rencredit.jschool.boruak.taskmanager;

import com.rencredit.jschool.boruak.taskmanager.bootstrap.Bootstrap;
import com.rencredit.jschool.boruak.taskmanager.config.DataBaseConfiguration;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class InitDB {

    public static void main(String[] args) throws DeniedAccessException, UnknownUserException {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(DataBaseConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.test();
    }

}

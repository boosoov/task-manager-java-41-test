package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.dto.CustomUser;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;

import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * http://localhost:8080/ws/ProjectEndpoint?wsdl
 */

@Component
@WebService
public class ProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @WebMethod
    public void createProject(
            @WebParam(name = "project") ProjectDTO project
    ) throws EmptyProjectException, EmptyUserIdException, EmptyLoginException, DeniedAccessException, UnknownUserException {
        projectService.create(UserUtil.getUserId(), project);
    }

    @WebMethod
    public ProjectDTO findProjectByIdDTO(
            @WebParam(name = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return projectService.findOneByUserIdAndProjectIdDTO(UserUtil.getUserId(), id);
    }

    @WebMethod
    public void deleteProject(
            @WebParam(name = "project") ProjectDTO project
    ) throws EmptyProjectException, EmptyUserIdException, DeniedAccessException, UnknownUserException {
        projectService.delete(UserUtil.getUserId(), project);
    }

    @WebMethod
    public void deleteProjectById(
            @WebParam(name = "id") String id
    ) throws DeniedAccessException, UnknownUserException {
        projectService.deleteByUserIdAndProjectId(UserUtil.getUserId(), id);
    }

    @WebMethod
    public List<ProjectDTO> getListProjects() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return projectService.findAllByUserIdDTO(UserUtil.getUserId());
    }

    @WebMethod
    public void deleteAllProjects() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        projectService.deleteAllByUserId(UserUtil.getUserId());
    }

    @WebMethod
    public boolean existsProjectById(
            @WebParam(name = "id") String id
    ) throws DeniedAccessException, UnknownUserException {
        return projectService.existsByUserIdAndProjectId(UserUtil.getUserId(), id);
    }

}

package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.dto.CustomUser;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class UserDetailServiceBean implements UserDetailsService {

    @Autowired
    UserService userService;

//    @Nullable
//    @Autowired
//    private ISessionService sessionService;

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        final UserDTO user = userService.getByLoginDTO(userName);
        if(user == null) throw new NotExistUserException();
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(new String[]{ })
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    private void init() throws EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException, EmptyRoleException {
        if(!userService.getListDTO().isEmpty()) return;
        userService.addUser("1", "1");
        userService.addUser("test", "test");
        userService.addUser("admin", "admin", Role.ADMIN);
//        sessionService.closeAll();
    }

}

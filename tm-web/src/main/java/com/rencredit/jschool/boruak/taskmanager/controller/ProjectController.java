package com.rencredit.jschool.boruak.taskmanager.controller;

import com.rencredit.jschool.boruak.taskmanager.dto.CustomUser;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Status;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @ModelAttribute("statuses")
    public Status[] getStatus() {
        return Status.values();
    }

    @GetMapping("/project/create")
    public String create(
            @AuthenticationPrincipal CustomUser user
            ) throws EmptyNameException, EmptyUserIdException {
        projectService.create(user.getUserId(), "Empty");
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") String id,
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyIdException, EmptyUserIdException {
        projectService.deleteOneById(user.getUserId(), id);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id,
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyIdException, EmptyUserIdException {
        final Project project = projectService.findOneEntityById(user.getUserId(), id);
        return new ModelAndView("project-edit", "project", project);
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @ModelAttribute("project") Project project,
            BindingResult result,
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyProjectException, EmptyUserIdException {
        @NotNull final User userById = userService.getByIdEntity(user.getUserId());
        project.setUser(userById);
        projectService.create(user.getUserId(), project);
        return "redirect:/projects";
    }

}

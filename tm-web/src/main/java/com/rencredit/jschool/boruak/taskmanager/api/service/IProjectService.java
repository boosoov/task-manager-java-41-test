package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectService extends IService<ProjectDTO> {

    void create(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    void create(@Nullable String userId, @Nullable String name, @Nullable String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException;

    void create(@Nullable final String userId, @Nullable ProjectDTO project) throws EmptyProjectException, EmptyUserIdException;

    void create(@Nullable final String userId, @Nullable Project project) throws EmptyProjectException, EmptyUserIdException;

    void delete(@Nullable String userId, @Nullable ProjectDTO project) throws EmptyProjectException, EmptyUserIdException;

    @NotNull
    List<ProjectDTO> findAllByUserIdDTO(@Nullable String userId) throws EmptyUserIdException;

    void deleteAllByUserId(@Nullable String userId) throws EmptyUserIdException;

    @Nullable
    ProjectDTO findOneByIdDTO(@Nullable String userId, @Nullable String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    ProjectDTO findOneByUserIdAndProjectIdDTO(@Nullable final String userId, @Nullable final String projectId) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    Project findOneEntityById(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    ProjectDTO findOneByIndexDTO(@Nullable String userId, @Nullable Integer index) throws IncorrectIndexException, EmptyUserIdException;

    @Nullable
    ProjectDTO findOneByNameDTO(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    @Nullable
    void deleteOneById(@Nullable String userId, @Nullable String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    void deleteOneByIndex(@Nullable String userId, @Nullable Integer index) throws IncorrectIndexException, EmptyUserIdException, EmptyProjectException;

    @Nullable
    void deleteOneByName(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    @NotNull
    ProjectDTO updateProjectById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws EmptyProjectException, EmptyNameException, EmptyIdException, EmptyUserIdException, EmptyDescriptionException;

    @NotNull
    ProjectDTO updateProjectByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws EmptyProjectException, EmptyNameException, IncorrectIndexException, EmptyUserIdException, EmptyDescriptionException;

    @NotNull List<ProjectDTO> getList();

    void deleteAll();

    <S extends ProjectDTO> List<S> saveAll(Iterable<S> iterable);

    long count();

    long countAllByUserId(@NotNull String userId);

    void deleteAll(Iterable<? extends ProjectDTO> iterable);

//    void deleteAllByUserId(@NotNull String userId, @NotNull List<Project> list);

    boolean existsById(String s);

    boolean existsByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}

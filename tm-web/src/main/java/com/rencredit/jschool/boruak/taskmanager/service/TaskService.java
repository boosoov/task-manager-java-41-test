package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.ITaskService;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import com.rencredit.jschool.boruak.taskmanager.repository.dto.ITaskRepositoryDTO;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.ITaskRepositoryEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService extends AbstractService<TaskDTO> implements ITaskService {

    @Autowired
    private ITaskRepositoryDTO repositoryDTO;

    @Autowired
    private ITaskRepositoryEntity repositoryEntity;

    @Override
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final TaskDTO task = new TaskDTO(userId, name);
        repositoryDTO.save(task);
    }

    @Override
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @NotNull final TaskDTO task = new TaskDTO(userId, name, description);
        repositoryDTO.save(task);
    }

    @Override
    @Transactional
    public void create(@Nullable final TaskDTO task) throws EmptyTaskException, EmptyUserIdException {
        if (task == null) throw new EmptyTaskException();

        repositoryDTO.save(task);
    }

    @Override
    @Transactional
    public void delete(@Nullable final TaskDTO task) throws EmptyTaskException, EmptyUserIdException {
        if (task == null) throw new EmptyTaskException();

        repositoryEntity.deleteById(task.getId());
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserIdDTO(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final List<TaskDTO> tasks = repositoryDTO.findAllByUserId(userId);
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull String userId) {
        return repositoryEntity.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectIdDTO(@Nullable final String projectId) throws EmptyProjectIdException {
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        @NotNull final List<TaskDTO> tasks = repositoryDTO.findAllByProject(projectId);
        return tasks;
    }

    @Override
    @Transactional
    public void deleteAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        repositoryEntity.deleteAllByUserId(userId);
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndexDTO(@Nullable final String userId, @Nullable final Integer index) throws EmptyUserIdException, IncorrectIndexException {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final TaskDTO task = findAllByUserIdDTO(userId).get(index);
        return task;
    }

    @Nullable
    @Override
    public TaskDTO findOneByNameDTO(@Nullable final String userId, @Nullable final String name) throws EmptyUserIdException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();


        @Nullable final TaskDTO task = repositoryDTO.findByUserIdAndName(userId, name);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, EmptyDescriptionException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @Nullable final TaskDTO task = findOneByIdDTO(userId, id);
        if (task == null) throw new EmptyTaskException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        repositoryDTO.save(task);
        return task;
    }

    @Override
    @Transactional
    public void attachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException {
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        @Nullable final TaskDTO task = findOneByIdDTO(userId, taskId);
        if (task == null) throw new EmptyTaskException();
        task.setProject(projectId);
        repositoryDTO.save(task);
    }

    @Override
    @Transactional
    public void detachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException {
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        @Nullable final TaskDTO task = findOneByIdDTO(userId, taskId);
        if (task == null) throw new EmptyTaskException();
        task.setProject(null);
        repositoryDTO.save(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws EmptyTaskException, EmptyNameException, EmptyUserIdException, IncorrectIndexException, EmptyDescriptionException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @Nullable final TaskDTO task = findOneByIndexDTO(userId, index);
        if (task == null) throw new EmptyTaskException();
        task.setName(name);
        task.setDescription(description);
        repositoryDTO.save(task);
        return task;
    }

    @Override
    @Transactional
    public void deleteOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException, EmptyTaskException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        @Nullable final TaskDTO task = findOneByIndexDTO(userId, index);
        if (task == null) throw new EmptyTaskException();

        @NotNull final String taskId= task.getId();
        repositoryEntity.deleteById(taskId);
    }

    @Override
    @Transactional
    public void deleteOneByName(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        repositoryEntity.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void deleteOneById(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        repositoryEntity.deleteById(id);
    }

    @Override
    @Nullable
    @Transactional
    public Task deleteByUserIdAndTaskId(@NotNull String userId, @NotNull String taskId) {
        return repositoryEntity.deleteByUserIdAndId(userId, taskId);
    }

    @Nullable
    @Override
    public TaskDTO findOneByIdDTO(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @Nullable final Optional<TaskDTO> opt = repositoryDTO.findById(id);
        return opt.isPresent() ? opt.get() : null;
    }

    @Nullable
    @Override
    public TaskDTO findOneByUserIdAndTaskIdDTO(@Nullable final String userId, @Nullable final String taskId) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();

        return repositoryDTO.findByUserIdAndId(userId, taskId);
    }

    @NotNull
    @Override
    public List<TaskDTO> getListDTO() {
        @NotNull final List<TaskDTO> tasks = repositoryDTO.findAll();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> getListEntity() {
        @NotNull final List<Task> tasks = repositoryEntity.findAll();
        return tasks;
    }

    @Override
    @Transactional
    public void deleteAll() {
        repositoryEntity.deleteAll();
    }

    @Transactional
    public <S extends TaskDTO> List<S> saveAll(Iterable<S> iterable) {
        return repositoryDTO.saveAll(iterable);
    }

    public long count() {
        return repositoryDTO.count();
    }

    public long countAllByUserId(@NotNull String userId) {
        return repositoryDTO.countAllByUserId(userId);
    }

    @Transactional
    public void deleteAll(Iterable<? extends Task> iterable) {
        repositoryEntity.deleteAll(iterable);
    }

//    @Transactional
//    public void deleteAllByUserId(@NotNull String userId, @NotNull List<Task> list) {
//        repositoryEntity.deleteAllByUserId(userId, list);
//    }

    public boolean existsById(String s) {
        return repositoryDTO.existsById(s);
    }

    public boolean existsByUserIdAndTaskId(@NotNull String userId, @NotNull String taskId) {
        return repositoryEntity.existsByUserIdAndId(userId, taskId);
    }

}

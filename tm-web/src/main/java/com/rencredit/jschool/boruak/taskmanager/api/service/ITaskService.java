package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskService extends IService<TaskDTO> {

    void create(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    void create(@Nullable String userId, @Nullable String name, @Nullable String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException;

    void create(@Nullable TaskDTO task) throws EmptyTaskException, EmptyUserIdException;

    void delete(@Nullable TaskDTO task) throws EmptyTaskException, EmptyUserIdException;

    @NotNull
    List<TaskDTO> findAllByUserIdDTO(@Nullable String userId) throws EmptyUserIdException;

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAllByProjectIdDTO(@Nullable final String projectId) throws EmptyProjectIdException;

    void deleteAllByUserId(@Nullable String userId) throws EmptyUserIdException;

    @Nullable
    TaskDTO findOneByIdDTO(@Nullable String userId, @Nullable String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    TaskDTO findOneByUserIdAndTaskIdDTO(@Nullable final String userId, @Nullable final String taskId) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    TaskDTO findOneByIndexDTO(@Nullable String userId, @Nullable Integer index) throws EmptyUserIdException, IncorrectIndexException;

    @Nullable
    TaskDTO findOneByNameDTO(@Nullable String userId, @Nullable String name) throws EmptyUserIdException, EmptyNameException;

    @Nullable
    void deleteOneById(@Nullable String userId, @Nullable String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    public Task deleteByUserIdAndTaskId(@NotNull String userId, @NotNull String taskId);

    @Nullable
    void deleteOneByIndex(@Nullable String userId, @Nullable Integer index) throws IncorrectIndexException, EmptyUserIdException, EmptyTaskException;

    @Nullable
    void deleteOneByName(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    @NotNull
    TaskDTO updateTaskById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, EmptyDescriptionException;

    void attachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException;

    void detachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException;

    @NotNull
    TaskDTO updateTaskByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws EmptyTaskException, EmptyNameException, EmptyUserIdException, IncorrectIndexException, EmptyDescriptionException;

    @NotNull List<TaskDTO> getListDTO();

    @NotNull
    List<Task> getListEntity();

    void deleteAll();

    <S extends TaskDTO> List<S> saveAll(Iterable<S> iterable);

    long count();

    void deleteAll(Iterable<? extends Task> iterable);

    boolean existsById(String s);

    boolean existsByUserIdAndTaskId(@NotNull String userId, @NotNull String taskId);

}

package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserService extends IService<UserDTO> {

    @Nullable
    UserDTO getByIdDTO(@Nullable String id) throws EmptyIdException;

    @Nullable
     User getByIdEntity(String s);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    UserDTO getByLoginDTO(@Nullable String login) throws EmptyLoginException;

    void add(@Nullable String login, @Nullable String password) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException;

    void add(@Nullable String login, @Nullable String password, @Nullable String firstName) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyFirstNameException, EmptyPasswordException;

    void add(@Nullable String login, @Nullable String password, @Nullable Role role) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyRoleException, EmptyPasswordException;

    void add(@Nullable final String login, @Nullable final UserDTO user) throws BusyLoginException, EmptyUserException, EmptyLoginException;

    void addUser(@Nullable final String login, @Nullable final String password) throws EmptyLoginException, EmptyPasswordException, EmptyHashLineException, DeniedAccessException, EmptyUserException, BusyLoginException;

    void addUser(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws EmptyLoginException, EmptyPasswordException, EmptyRoleException, EmptyHashLineException, DeniedAccessException, BusyLoginException, EmptyUserException;

    void addUserEntity(@Nullable final User user) throws EmptyLoginException, EmptyUserException, BusyLoginException;

    @Nullable
    UserDTO editProfileById(@Nullable String id, @Nullable String firstName) throws EmptyUserException, EmptyFirstNameException, EmptyIdException;

    @Nullable
    UserDTO editProfileById(@Nullable String id, @Nullable String firstName, @Nullable String lastName) throws EmptyUserException, EmptyLastNameException, EmptyFirstNameException, EmptyIdException;

    @Nullable
    UserDTO editProfileById(@Nullable String id, @Nullable final String email, @Nullable String firstName, @Nullable String lastName, @Nullable final String middleName) throws EmptyUserException, EmptyMiddleNameException, EmptyLastNameException, EmptyFirstNameException, EmptyEmailException, EmptyIdException;

    @Nullable
    UserDTO updatePasswordById(@Nullable String id, @Nullable String newPassword) throws EmptyUserException, IncorrectHashPasswordException, EmptyHashLineException, EmptyNewPasswordException, EmptyIdException;

    void removeById(@Nullable String id) throws EmptyIdException;

    void removeByLogin(@Nullable String login) throws EmptyLoginException;

    void removeByUser(@Nullable UserDTO user) throws EmptyUserException, EmptyIdException;

    @Nullable
    UserDTO lockUserByLogin(@Nullable String login) throws EmptyUserException, EmptyLoginException;

    @Nullable
    UserDTO unlockUserByLogin(@Nullable String login) throws EmptyUserException, EmptyLoginException;

    @NotNull List<UserDTO> getListDTO();

    @Override
    void deleteAll() throws EmptyPasswordException, EmptyUserException, EmptyLoginException, DeniedAccessException, BusyLoginException, EmptyHashLineException, EmptyRoleException;

}

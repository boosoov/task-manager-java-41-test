package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.repository.dto.IUserRepositoryDTO;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IUserRepositoryEntity;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserService extends AbstractService<UserDTO> implements IUserService {

    @Autowired
    private IUserRepositoryEntity repositoryEntity;

    @Autowired
    private IUserRepositoryDTO repositoryDTO;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void add(@Nullable final String login, @Nullable final String password) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @Nullable final String passwordHash = passwordEncoder.encode(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final UserDTO user = new UserDTO(login, passwordHash);
        add(login, user);
    }

    @Override
    @Transactional
    public void add(@Nullable final String login, @Nullable final String password, @Nullable final String firstName) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyFirstNameException, EmptyPasswordException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();

        @Nullable final String passwordHash = passwordEncoder.encode(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final UserDTO user = new UserDTO(login, passwordHash, firstName);
        add(login, user);
    }

    @Override
    @Transactional
    public void add(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyRoleException, EmptyPasswordException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        @Nullable final String passwordHash = passwordEncoder.encode(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final UserDTO user = new UserDTO(login, passwordHash, role);
        add(login, user);
    }

    @Transactional
    public void add(@Nullable final String login, @Nullable final UserDTO user) throws BusyLoginException, EmptyUserException, EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (user == null) throw new EmptyUserException();

        if (repositoryDTO.findByLogin(login) != null) throw new BusyLoginException();
        repositoryDTO.save(user);
    }


    @Override
    @Transactional
    public void addUser(@Nullable final String login, @Nullable final String password) throws EmptyLoginException, EmptyPasswordException, EmptyHashLineException, DeniedAccessException, EmptyUserException, BusyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @Nullable final String passwordHash = passwordEncoder.encode(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final User user = new User(login, passwordHash);
        addUserEntity(user);
    }

    @Override
    @Transactional
    public void addUser(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws EmptyLoginException, EmptyPasswordException, EmptyRoleException, EmptyHashLineException, DeniedAccessException, BusyLoginException, EmptyUserException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        @Nullable final String passwordHash = passwordEncoder.encode(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final User user = new User(login, passwordHash, role);
        addUserEntity(user);
    }

    @Override
    @Transactional
    public void addUserEntity(@Nullable final User user) throws EmptyUserException, BusyLoginException, EmptyLoginException {
        if (user == null) throw new EmptyUserException();
        if (user.getLogin() == null || user.getLogin().isEmpty()) throw new EmptyLoginException();

        if (repositoryEntity.findByLogin(user.getLogin()) != null) throw new BusyLoginException();
        repositoryEntity.save(user);
    }

    @Nullable
    @Override
    public UserDTO getByIdDTO(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @Nullable final Optional<UserDTO> opt = repositoryDTO.findById(id);
        return opt.isPresent() ? opt.get() : null;
    }

    @Nullable
    @Override
    public User getByIdEntity(String s) {
        return repositoryEntity.getOne(s);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull String login) {
        return repositoryEntity.findByLogin(login);
    }

    @Nullable
    @Override
    public UserDTO getByLoginDTO(@Nullable final String login) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return repositoryDTO.findByLogin(login);
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO updatePasswordById(@Nullable final String id, @Nullable final String newPassword) throws EmptyUserException, IncorrectHashPasswordException, EmptyHashLineException, EmptyNewPasswordException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyNewPasswordException();

        @Nullable final String passwordHash = passwordEncoder.encode(newPassword);
        if (passwordHash == null) throw new IncorrectHashPasswordException();
        @Nullable final UserDTO user = getByIdDTO(id);
        if (user == null) throw new EmptyUserException();
        user.setPasswordHash(passwordHash);
        repositoryDTO.save(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO editProfileById(@Nullable final String id, @Nullable final String firstName) throws EmptyUserException, EmptyFirstNameException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();

        @Nullable final UserDTO user = getByIdDTO(id);
        if (user == null) throw new EmptyUserException();
        user.setFirstName(firstName);
        repositoryDTO.save(user);
        return user;

    }

    @Nullable
    @Override
    @Transactional
    public UserDTO editProfileById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) throws EmptyUserException, EmptyLastNameException, EmptyFirstNameException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();

        @Nullable final UserDTO user = getByIdDTO(id);
        if (user == null) throw new EmptyUserException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        repositoryDTO.save(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO editProfileById(
            @Nullable final String id,
            @Nullable final String email,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws EmptyUserException, EmptyMiddleNameException, EmptyLastNameException, EmptyFirstNameException, EmptyEmailException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();

        @Nullable final UserDTO user = getByIdDTO(id);
        if (user == null) throw new EmptyUserException();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        repositoryDTO.save(user);
        return user;
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        repositoryEntity.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        repositoryEntity.deleteByLogin(login);
    }

    @Override
    @Transactional
    public void removeByUser(@Nullable final UserDTO user) throws EmptyUserException, EmptyIdException {
        if (user == null) throw new EmptyUserException();

        removeById(user.getId());
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO lockUserByLogin(@Nullable final String login) throws EmptyUserException, EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable final UserDTO user = getByLoginDTO(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(true);
        repositoryDTO.save(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO unlockUserByLogin(@Nullable final String login) throws EmptyUserException, EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable final UserDTO user = getByLoginDTO(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(false);
        repositoryDTO.save(user);
        return user;
    }

    @NotNull
    @Override
    public List<UserDTO> getListDTO() {
        @NotNull final List<UserDTO> list = repositoryDTO.findAll();
        return list;
    }

    @Override
    @Transactional
    public void deleteAll() throws EmptyPasswordException, EmptyUserException, EmptyLoginException, DeniedAccessException, BusyLoginException, EmptyHashLineException, EmptyRoleException {
        repositoryEntity.deleteAll();
        add("admin", "admin", Role.ADMIN);
        add("test", "test");
        add("1", "1");
    }


}

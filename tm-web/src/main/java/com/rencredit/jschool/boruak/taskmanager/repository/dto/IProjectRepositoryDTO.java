package com.rencredit.jschool.boruak.taskmanager.repository.dto;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Repository
public interface IProjectRepositoryDTO extends IAbstractRepositoryDTO<ProjectDTO> {

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull String userId);

    @Nullable
    ProjectDTO findByUserIdAndName(@NotNull String userId, @NotNull String name);

    @Nullable
    ProjectDTO findByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countAllByUserId(@NotNull String userId);

}

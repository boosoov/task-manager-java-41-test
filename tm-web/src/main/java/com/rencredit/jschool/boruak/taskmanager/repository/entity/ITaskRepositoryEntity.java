package com.rencredit.jschool.boruak.taskmanager.repository.entity;

import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITaskRepositoryEntity extends IAbstractRepositoryEntity<Task> {

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllByProject(@NotNull String projectId);

    @NotNull
    List<Task> deleteAllByUserId(@NotNull String userId);

    @Nullable
    Task deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

//    List<Task> deleteAllByUserId(@NotNull String userId, @NotNull List<Task> list);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}

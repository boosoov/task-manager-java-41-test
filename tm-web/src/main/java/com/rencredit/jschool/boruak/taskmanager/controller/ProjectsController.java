package com.rencredit.jschool.boruak.taskmanager.controller;

import com.rencredit.jschool.boruak.taskmanager.dto.CustomUser;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/projects")
    public ModelAndView index(@AuthenticationPrincipal CustomUser user) throws EmptyUserIdException {
        final List<ProjectDTO> projects = projectService.findAllByUserIdDTO(user.getUserId());
        return new ModelAndView("project-list", "projects", projects);
    }

}

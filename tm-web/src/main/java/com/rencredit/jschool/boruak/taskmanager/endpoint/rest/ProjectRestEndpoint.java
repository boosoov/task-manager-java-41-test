package com.rencredit.jschool.boruak.taskmanager.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IProjectRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @PostMapping
    public void create(@RequestBody ProjectDTO project) throws EmptyProjectException, EmptyUserIdException, EmptyLoginException, DeniedAccessException, UnknownUserException {
        projectService.create(UserUtil.getUserId(), project);
    }

    @Override
    @Nullable
    @GetMapping("/${id}")
    public ProjectDTO findOneByIdDTO(@PathVariable("id") String id) throws EmptyIdException, EmptyUserIdException, EmptyLoginException, DeniedAccessException, UnknownUserException {
        return projectService.findOneByUserIdAndProjectIdDTO(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/exist/${id}")
    public boolean existsById(@PathVariable("id") String id) throws DeniedAccessException, UnknownUserException {
        return projectService.existsByUserIdAndProjectId(UserUtil.getUserId(), id);
    }

    @Override
    @DeleteMapping("/${id}")
    public void deleteOneById(@PathVariable("id") String id) throws DeniedAccessException, UnknownUserException {
        projectService.deleteByUserIdAndProjectId(UserUtil.getUserId(), id);
    }

}

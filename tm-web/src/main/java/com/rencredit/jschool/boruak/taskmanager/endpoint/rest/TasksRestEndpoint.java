package com.rencredit.jschool.boruak.taskmanager.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.ITasksRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TasksRestEndpoint implements ITasksRestEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @GetMapping
    public List<TaskDTO> getListDTO() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return taskService.findAllByUserIdDTO(UserUtil.getUserId());
    }

    @Override
    @PostMapping
    public List<TaskDTO> saveAll(@RequestBody List<TaskDTO> list) throws DeniedAccessException, UnknownUserException {
        for (TaskDTO task : list) {
            task.setUserId(UserUtil.getUserId());
        }
        return taskService.saveAll(list);
    }

    @Override
    @GetMapping("/count")
    public long count() throws DeniedAccessException, UnknownUserException {
        return taskService.countAllByUserId(UserUtil.getUserId());
    }

    @Override
    @DeleteMapping("/all")
    public void deleteAll() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        taskService.deleteAllByUserId(UserUtil.getUserId());
    }

//    @Override
//    @DeleteMapping
//    public void deleteAll(@RequestBody List<Task> list) {
//        taskService.deleteAllByUserId(UserUtil.getUserId(), list);
//    }

}

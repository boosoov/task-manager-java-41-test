package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.dto.status.Result;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * http://localhost:8080/ws/AuthEndpoint?wsdl
 */

@Component
@WebService
public class AuthEndpoint {

    @Autowired
    private UserService userService;

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private ApplicationContext context;

    @WebMethod
    public Result login(
            @WebParam(name = "username") final String username,
            @WebParam(name = "password") final String password
    ) {
        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(username, password);
//        final AuthenticationManager authenticationManager = context.getBean(AuthenticationManager.class);
        final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new Result(authentication.isAuthenticated());
    }

    @WebMethod
    public User profile() throws DeniedAccessException, UnknownUserException {
        System.out.println("\n\n" + "profile()" + "\n\n");
        System.out.println("\n\n" + userService.getByIdEntity(UserUtil.getUserId()) + "\n\n");
        return userService.getByIdEntity(UserUtil.getUserId());
    }


    @WebMethod
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }


}

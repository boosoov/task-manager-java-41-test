package com.rencredit.jschool.boruak.taskmanager.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IProjectsRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectsRestEndpoint implements IProjectsRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @GetMapping
    public List<ProjectDTO> getListDTO() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return projectService.findAllByUserIdDTO(UserUtil.getUserId());
    }

    @Override
    @PostMapping
    public List<ProjectDTO> saveAll(@RequestBody List<ProjectDTO> list) throws DeniedAccessException, UnknownUserException {
        for (ProjectDTO project : list) {
            project.setUserId(UserUtil.getUserId());
        }
        return projectService.saveAll(list);
    }

    @Override
    @GetMapping("/count")
    public long count() throws DeniedAccessException, UnknownUserException {
        return projectService.countAllByUserId(UserUtil.getUserId());
    }

    @Override
    @DeleteMapping("/all")
    public void deleteAll() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        projectService.deleteAllByUserId(UserUtil.getUserId());
    }

//    @Override
//    @DeleteMapping
//    public void deleteAll(@RequestBody List<Project> list) {
//        projectService.deleteAllByUserId(UserUtil.getUserId(), list);
//    }

}

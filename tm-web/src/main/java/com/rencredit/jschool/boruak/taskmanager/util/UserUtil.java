package com.rencredit.jschool.boruak.taskmanager.util;

import com.rencredit.jschool.boruak.taskmanager.dto.CustomUser;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import javax.xml.ws.BindingProvider;
import java.util.Map;
import java.util.Objects;

public class UserUtil {

    public static String getUserId() throws DeniedAccessException, UnknownUserException {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new DeniedAccessException();

//        System.out.println("\n\n" + "UserUtil" + "\n\n");
//        System.out.println("\n\n" + principal + "\n\n");


        if(!(principal instanceof CustomUser)) throw new UnknownUserException();
//        if ("anonymousUser".equals(principal)) throw new UnknownUserException();
        final CustomUser customUser = (CustomUser)principal;
        return customUser.getUserId();

    }



}

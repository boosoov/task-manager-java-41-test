package com.rencredit.jschool.boruak.taskmanager.repository.entity;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IProjectRepositoryEntity extends IAbstractRepositoryEntity<Project> {

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Project> deleteAllByUserId(@NotNull String userId);

    @Nullable
    Project deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project findByUserIdAndName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

//    List<Project> deleteAllByUserId(@NotNull String userId, @NotNull List<Project> list);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}

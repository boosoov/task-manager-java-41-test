package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    void registration(@Nullable String login, @Nullable String password) throws EmptyPasswordException, EmptyLoginException, EmptyUserException, BusyLoginException, DeniedAccessException, EmptyHashLineException;

    void registration(@Nullable String login, @Nullable String password, @Nullable String firstName) throws EmptyPasswordException, EmptyFirstNameException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, EmptyEmailException;

    void registration(@Nullable String login, @Nullable String password, @Nullable Role role) throws EmptyRoleException, EmptyPasswordException, EmptyLoginException, EmptyUserException, BusyLoginException, DeniedAccessException, EmptyHashLineException;

}

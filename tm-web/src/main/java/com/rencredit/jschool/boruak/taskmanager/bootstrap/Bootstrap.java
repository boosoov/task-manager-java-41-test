package com.rencredit.jschool.boruak.taskmanager.bootstrap;

import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Bootstrap {

    @Nullable
    @Autowired
    private IUserService userService;


    public Bootstrap() {
    }

    public void init() {
//        try {
////            initUsers();
//        } catch (EmptyHashLineException e) {
//            e.printStackTrace();
//        } catch (BusyLoginException e) {
//            e.printStackTrace();
//        } catch (EmptyPasswordException e) {
//            e.printStackTrace();
//        } catch (EmptyLoginException e) {
//            e.printStackTrace();
//        } catch (EmptyUserException e) {
//            e.printStackTrace();
//        } catch (DeniedAccessException e) {
//            e.printStackTrace();
//        } catch (EmptyRoleException e) {
//            e.printStackTrace();
//        }
//        sessionService.closeAll();
    }

    public void test() throws DeniedAccessException, UnknownUserException {
        System.out.println("\n\n" + userService.getByIdEntity(UserUtil.getUserId()) + "\n\n");
    }


//    private void initUsers() throws EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException, EmptyRoleException {
//        userService.addUser("1", "1");
//        userService.addUser("test", "test");
//        userService.addUser("admin", "admin", Role.ADMIN);
//    }

}

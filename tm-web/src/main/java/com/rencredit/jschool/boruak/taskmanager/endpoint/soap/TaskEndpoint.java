package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * http://localhost:8080/ws/TaskEndpoint?wsdl
 */

@Component
@WebService
public class TaskEndpoint {

    @Autowired
    private TaskService taskService;

    @WebMethod
    public void createTask(
            @WebParam(name = "task") TaskDTO task
    ) throws EmptyTaskException, EmptyUserIdException, DeniedAccessException, UnknownUserException {
        task.setUserId(UserUtil.getUserId());
        taskService.create(task);
    }

    @WebMethod
    public TaskDTO findTaskByIdDTO(
            @WebParam(name = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return taskService.findOneByUserIdAndTaskIdDTO(UserUtil.getUserId(), id);
    }

    @WebMethod
    public void deleteTask(
            @WebParam(name = "task") TaskDTO task
    ) throws EmptyUserIdException, EmptyTaskException {
        taskService.delete(task);
    }

    @WebMethod
    public void deleteTaskById(
            @WebParam(name = "id") String id
    ) throws DeniedAccessException, UnknownUserException {
        taskService.deleteByUserIdAndTaskId(UserUtil.getUserId(), id);
    }

    @WebMethod
    public List<TaskDTO> getListTasks() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return taskService.findAllByUserIdDTO(UserUtil.getUserId());
    }

    @WebMethod
    public void deleteAllTasks() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        taskService.deleteAllByUserId(UserUtil.getUserId());
    }

    @WebMethod
    public boolean existsTaskById(
            @WebParam(name = "id") String id
    ) throws DeniedAccessException, UnknownUserException {
        return taskService.existsByUserIdAndTaskId(UserUtil.getUserId(), id);
    }

}

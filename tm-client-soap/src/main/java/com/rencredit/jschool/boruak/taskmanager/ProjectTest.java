package com.rencredit.jschool.boruak.taskmanager;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import javax.xml.ws.BindingProvider;
import java.util.Map;

public class ProjectTest {


    public static void main(String[] args) throws EmptyUserIdException_Exception, UnknownUserException_Exception, DeniedAccessException_Exception {
//        System.out.println(projectEndpoint.getListProjects());


        ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        AuthEndpointService authEndpointService = new AuthEndpointService();
        ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();
        AuthEndpoint authEndpoint= authEndpointService.getAuthEndpointPort();

        setMaintain(authEndpoint);


        System.out.println(authEndpoint.login("test", "test"));
        System.out.println(authEndpoint.login("test", "test").isSuccess());
        System.out.println(authEndpoint.profile());
        System.out.println(authEndpoint.profile().getLogin());
    }

    public static void setMaintain(final Object port) {
        final BindingProvider bindingProvider = (BindingProvider) port;
        final Map<String, Object> map = bindingProvider.getRequestContext();
        map.put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
    }

}

package com.rencredit.jschool.boruak.taskmanager.config;


import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.rencredit.jschool.boruak.taskmanager")
public class ClientConfiguration {
    
//    @Bean
//    public UserEndpointService userEndpointService() { return new UserEndpointService(); }
//
//    @Bean
//    public SessionEndpointService sessionEndpointService() { return new SessionEndpointService(); }
//
    @Bean
    public TaskEndpointService taskEndpointService() { return new TaskEndpointService(); }

    @Bean
    public ProjectEndpointService projectEndpointService() { return new ProjectEndpointService(); }

//    @Bean
//    public AdminEndpointService adminEndpointService() { return new AdminEndpointService(); }
//
//    @Bean
//    public AdminUserEndpointService adminUserEndpointService() { return new AdminUserEndpointService(); }
//
    @Bean
    public AuthEndpointService authEndpointService() { return new AuthEndpointService(); }
//
//
//    @Bean
//    public UserEndpoint userEndpoint(
//            @NotNull @Autowired final UserEndpointService userEndpointService
//    ) { return userEndpointService.getUserEndpointPort(); }
//
//    @Bean
//    public SessionEndpoint sessionEndpoint(
//            @NotNull @Autowired final SessionEndpointService sessionEndpointService
//    ) { return sessionEndpointService.getSessionEndpointPort(); }
//
    @Bean
    public TaskEndpoint taskEndpoint(
            @NotNull @Autowired final TaskEndpointService taskEndpointService
    ) { return taskEndpointService.getTaskEndpointPort(); }

    @Bean
    public ProjectEndpoint projectEndpoint(
            @NotNull @Autowired final ProjectEndpointService projectEndpointService
    ) { return projectEndpointService.getProjectEndpointPort(); }

//    @Bean
//    public AdminEndpoint adminEndpoint(
//            @NotNull @Autowired final AdminEndpointService adminEndpointService
//    ) { return adminEndpointService.getAdminEndpointPort(); }
//
//    @Bean
//    public AdminUserEndpoint adminUserEndpoint(
//            @NotNull @Autowired final AdminUserEndpointService adminUserEndpointService
//    ) { return adminUserEndpointService.getAdminUserEndpointPort(); }
//
    @Bean
    public AuthEndpoint authEndpoint(
            @NotNull @Autowired final AuthEndpointService authEndpointService
    ) { return authEndpointService.getAuthEndpointPort(); }

}

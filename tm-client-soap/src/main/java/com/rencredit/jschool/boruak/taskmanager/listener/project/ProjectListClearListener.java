package com.rencredit.jschool.boruak.taskmanager.listener.project;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.DeniedAccessException_Exception;
import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.EmptyUserIdException_Exception;
import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.ProjectEndpoint;
import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.UnknownUserException_Exception;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ProjectListClearListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    @EventListener(condition = "@projectListClearListener.name() == #event.command")
    public void handle(final ConsoleEvent event) throws EmptyUserIdException_Exception, UnknownUserException_Exception, DeniedAccessException_Exception {
        System.out.println("[CLEAR PROJECTS]");

        projectEndpoint.deleteAllProjects();
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}

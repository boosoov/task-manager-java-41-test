
package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.rencredit.jschool.boruak.taskmanager.endpoint.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Login_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "login");
    private final static QName _LoginResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "loginResponse");
    private final static QName _Logout_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "logout");
    private final static QName _LogoutResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "logoutResponse");
    private final static QName _Profile_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "profile");
    private final static QName _ProfileResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "profileResponse");
    private final static QName _UnknownUserException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "UnknownUserException");
    private final static QName _DeniedAccessException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "DeniedAccessException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.rencredit.jschool.boruak.taskmanager.endpoint.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link Logout }
     * 
     */
    public Logout createLogout() {
        return new Logout();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     * 
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    /**
     * Create an instance of {@link Profile }
     * 
     */
    public Profile createProfile() {
        return new Profile();
    }

    /**
     * Create an instance of {@link ProfileResponse }
     * 
     */
    public ProfileResponse createProfileResponse() {
        return new ProfileResponse();
    }

    /**
     * Create an instance of {@link UnknownUserException }
     * 
     */
    public UnknownUserException createUnknownUserException() {
        return new UnknownUserException();
    }

    /**
     * Create an instance of {@link DeniedAccessException }
     * 
     */
    public DeniedAccessException createDeniedAccessException() {
        return new DeniedAccessException();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link AbstractEntity }
     * 
     */
    public AbstractEntity createAbstractEntity() {
        return new AbstractEntity();
    }

    /**
     * Create an instance of {@link Project }
     * 
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Login }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "login")
    public JAXBElement<Login> createLogin(Login value) {
        return new JAXBElement<Login>(_Login_QNAME, Login.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Logout }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "logout")
    public JAXBElement<Logout> createLogout(Logout value) {
        return new JAXBElement<Logout>(_Logout_QNAME, Logout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogoutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "logoutResponse")
    public JAXBElement<LogoutResponse> createLogoutResponse(LogoutResponse value) {
        return new JAXBElement<LogoutResponse>(_LogoutResponse_QNAME, LogoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Profile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "profile")
    public JAXBElement<Profile> createProfile(Profile value) {
        return new JAXBElement<Profile>(_Profile_QNAME, Profile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "profileResponse")
    public JAXBElement<ProfileResponse> createProfileResponse(ProfileResponse value) {
        return new JAXBElement<ProfileResponse>(_ProfileResponse_QNAME, ProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnknownUserException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "UnknownUserException")
    public JAXBElement<UnknownUserException> createUnknownUserException(UnknownUserException value) {
        return new JAXBElement<UnknownUserException>(_UnknownUserException_QNAME, UnknownUserException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeniedAccessException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "DeniedAccessException")
    public JAXBElement<DeniedAccessException> createDeniedAccessException(DeniedAccessException value) {
        return new JAXBElement<DeniedAccessException>(_DeniedAccessException_QNAME, DeniedAccessException.class, null, value);
    }

}
